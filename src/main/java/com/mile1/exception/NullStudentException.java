package com.mile1.exception;

public class NullStudentException extends Exception {
    @Override
    public String toString() {
        return getClass().getSimpleName() + " has occurred";
    }
}
