package com.mile1.bean;

public class Student {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int[] getMarks() {
        return marks;
    }

    public void setMarks(int[] marks) {
        this.marks = marks;
    }

    private int []marks;

    public Student(String name, int[] marks) {
        this.name = name;
        this.marks = marks;
    }
}
