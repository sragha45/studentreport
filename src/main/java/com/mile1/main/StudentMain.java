package com.mile1.main;

import com.mile1.bean.Student;
import com.mile1.com.mile1.service.StudentReport;
import com.mile1.com.mile1.service.StudentService;
import com.mile1.exception.NullMarksArrayException;
import com.mile1.exception.NullNameException;
import com.mile1.exception.NullStudentException;

public class StudentMain {
    static Student[] data = new Student[4];

    static {
        data[0] = new Student("Sekar", new int[]{35, 35, 35});
        data[1] = new Student(null, new int[]{11, 22, 33});
        data[2] = null;
        data[3] = new Student("Manoj", null);
    }

    public static void main(String[] args) {
        StudentReport studentReport = new StudentReport();

        for (Student student : data) {
            try {
                System.out.println(studentReport.validate(student));
            } catch (NullMarksArrayException | NullNameException | NullStudentException e) {
                System.out.println(e);
            }
        }

        StudentService studentService = new StudentService();

        System.out.println("Number of null marks: " +
                studentService.findNumberOfNullMarks(data));

        System.out.println("Number of null names: " +
                studentService.findNumberOfNullNames(data));

        System.out.println("Number of null objects: " +
                studentService.findNumberOfNullObjects(data));

    }
}
