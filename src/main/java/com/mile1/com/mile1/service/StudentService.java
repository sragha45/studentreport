package com.mile1.com.mile1.service;

import com.mile1.bean.Student;
import com.mile1.exception.NullMarksArrayException;
import com.mile1.exception.NullNameException;
import com.mile1.exception.NullStudentException;

public class StudentService {
    private int findFaults(Student []students, Exception e) {

        int faulty = 0;
        for (Student student : students) {
            try {
                new StudentReport().validate(student);
            } catch (Throwable throwable) {
                if(throwable.getClass().equals(e.getClass())) {
                    faulty += 1;
                }
            }
        }

        return faulty;
    }
    public int findNumberOfNullMarks(Student []students) {
        return findFaults(students, new NullMarksArrayException());
    }

    public int findNumberOfNullNames(Student[] students) {
        return findFaults(students, new NullNameException());
    }

    public int findNumberOfNullObjects(Student[] students) {
        return findFaults(students, new NullStudentException());
    }



}
